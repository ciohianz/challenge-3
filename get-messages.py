#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import argparse
from DBManager import DBManager
from ConfigManager import ConfigManager
from ErrorHandler import ErrorHandler

import imaplib
from email import message_from_string, utils
from email.header import decode_header
import time

import logging
logging.basicConfig(filename='output.log',level=logging.DEBUG)


# imap server to make queries
IMAP_SERVER = "imap.gmail.com"

# Config Manager to read config.ini
config = ConfigManager()

# DBManager to execute database queries
dbManager = DBManager(config)

# easier way to handle errors
errorHandler = ErrorHandler(logging)

# parsing arguments
parser = argparse.ArgumentParser(description='Tool for reading GMail inbox')
parser.add_argument('input', type=str,
                    help='CSV file with GMail accounts credentials.')
parser.add_argument('-b','--body', 
                    help='Filter by email body', required=False)
parser.add_argument('-s','--subject', 
                    help='Filter by email subject', required=False)


try:
    parsed_args = parser.parse_args()
except IOError, msg:
    parser.error(str(msg))

def storeUser(username):
    stmt = "INSERT INTO users (email) VALUES ('{}')".format(username)

    try:
        dbManager.execute(stmt)
    except Exception, e:
        dbManager.rollback()
        msg = "DBManager Error ({}): {}".format(e[0], e[1])
        errorHandler.handle(msg)
    else:
        dbManager.commit()
        return findUser(username)


def findUser(username):
    stmt = "SELECT * FROM users WHERE email='{}'".format(username)

    try:
        if dbManager.execute(stmt):
            return dbManager.fetchone()
    except Exception, e:
        msg = "DBManager Error (): {}".format(e[0], e[1])
        errorHandler.handle(msg)

    return None


def getUserOrStore(username):
    user = findUser( username )

    if user == None:
        user = storeUser( username )

    return user


def messageAlreadyExists(message_id):
    stmt = "SELECT * FROM messages WHERE message_id = {}".format(message_id)

    try:
        return dbManager.execute(stmt)
    except Exception, e:
        msg = "DBManager Error ({}): {}".format(e[0], e[1])
        errorHandler.handle(msg)


def storeMessage(message, user):
    stmt = ("INSERT INTO messages (message_id, fecha, efrom, subject, "
            "user_id) VALUES (%s,%s,%s,%s,%s)")
    params = (
            message['id'],
            message['Date'],
            message['From'],
            message['Subject'],
            user[0]
        )

    try:
        dbManager.executePrepared( stmt, params )
    except Exception, e:
        dbManager.rollback()
        msg = "DBManager Error ({}): {}".format(e[0], e[1])
        errorHandler.handle(msg)
    else:
        dbManager.commit()
        print "Message stored: [{}]".format( message['id'] )


def table_exists(table):
    stmt = ("select * from information_schema.tables where "
            "table_schema = '{}' and table_name = '{}'")

    database = config.get('database', 'DB_DATABASE')

    return dbManager.execute( stmt.format(database, table) )


def create_users_table():
    stmt = ('CREATE TABLE users('
            'id int(11) unsigned AUTO_INCREMENT,'
            'email char(255) NOT NULL,'
            'created_at timestamp DEFAULT CURRENT_TIMESTAMP,'
            'primary key (id),'
            'UNIQUE (email)'
            ');')
    
    dbManager.execute(stmt)
    print "Users table created successfully!"


def create_messages_table():
    stmt = ('CREATE TABLE messages('
            'id int(11) unsigned AUTO_INCREMENT,'
            'message_id varchar(250) NOT NULL,'
            'fecha bigint unsigned NOT NULL,'
            'efrom char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci'
            ' NOT NULL,'
            'subject char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci'
            ' NOT NULL,'
            'user_id int(11) unsigned NOT NULL,'
            'foreign key (user_id) references users(id),'
            'primary key (id)'
            ') ENGINE=InnoDB;')

    dbManager.execute(stmt)
    print "Messages table created successfully!"


def check_tables():
    database = config.get('database', 'DB_DATABASE')

    try:
        dbManager.select_db(database)
    except Exception, e:
        try:
            stmt = "CREATE DATABASE {};".format(database) 
            dbManager.execute(stmt)
            dbManager.select_db(database);
        except Exception, e:
            raise e

    if not table_exists('users'):
        create_users_table()

    if not table_exists('messages'):
        create_messages_table()


def login_user(username, pwd):
    print "\nLogin user {} ...".format(username)
    try:
        mail = imaplib.IMAP4_SSL(IMAP_SERVER)
        mail.login(username, pwd)
    except Exception, e:
        msg = "Auth Error ({}): {}".format(username, e)
        errorHandler.handle(msg)
        return None
    else:
        print "OK!"
        return mail


def extract_data(data):
    # data to return
    output = {}

    # load message
    msg = message_from_string(data)

    # decode From header
    efrom = decode_header( msg['From'] )
    if len(efrom) == 1:
        email_regex = r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)"
        matches = re.findall( email_regex, efrom[0][0] )
        output['From'] = matches[-1]
    else:
        output['From'] = efrom[1][0][1:-1]

    # decode Subject header
    subject = decode_header( msg['Subject'] )[0][0]
    try:
        output['Subject'] = subject.decode('utf-8')
    except Exception, error:
        try:
            output['Subject'] = subject.decode('latin1')
        except Exception, e:
            output['Subject'] = subject

    # decode Date header
    date = decode_header( msg['Date'] )[0][0]

    # convert Date header to timestamp for storage
    output['Date'] = time.mktime( utils.parsedate(date) )
    
    return output


def parseFilters():
    filters = ()
    if parsed_args.body:
        filters = filters + ('(BODY "'+parsed_args.body+'")',)

    if parsed_args.subject:
        filters = filters + ('(SUBJECT "'+parsed_args.subject+'")',)

    return filters if len(filters) > 0 else ['All']


def main():

    # check if tables exist
    try:
        check_tables()
    except Exception, e:
        msg = "DBManager Error ({}): {}".format(e[0], e[1])
        errorHandler.handle(msg, exitProgram=True)

    # open csv file with accounts credentials
    inputFile = parsed_args.input
    try:
        handler = open(inputFile, 'r')
    except Exception, e:
        errorHandler.handle("File Error: {}".format(e), exitProgram=True)

    # filter emails
    filters = parseFilters()
    for accountData in handler:
        username, pwd = accountData.split(",")

        # login user using imap
        mail = login_user(username, pwd)
        if mail:

            # get/store logged in user into DB
            user = getUserOrStore( username )
            if user:
                # select which inbox to read from
                typ, result = mail.select('inbox')
                if typ != 'OK':
                    msg = "Inbox Error: {} - {}".format(username, result)
                    errorHandler.handle(msg)
                else:
                    try:
                        typ, data = mail.search(None, *filters)
                    except Exception, e:
                        msg = "Search Error: {}".format(e)
                        errorHandler.handle(msg)
                    else:
                        if typ != "OK":
                            msg = "Error filtering emails " \
                                  "for {}".format(username)
                            errorHandler.handle(msg)
                        else:
                            # parse message ids
                            mail_ids = data[0]
                            id_list = mail_ids.split()

                            '''
                                iterate each message_id to fetch the whole
                                email body and headers to process them
                            '''
                            for i in id_list:
                                typ, data = mail.fetch(i, '(RFC822)')
                                if typ != "OK":
                                    msg = "Error fetching email [{}] " \
                                          "for {}".format(i, username)
                                    errorHandler.handle(msg)
                                else:
                                    # parse email headers
                                    ex_data = extract_data(data[0][1])
                                    ex_data['id'] = i

                                    # persist message data
                                    if not messageAlreadyExists(i):
                                        storeMessage(ex_data, user)
                                    else:
                                        print "Message [{}] already " \
                                              "exists".format(i)
        print "\n--------------------------------------------------\n"

    try:
        dbManager.close()
        handler.close()
    except Exception, e:
        errorHandler.handle("Error: {}".format(e))

    print "\nAll messages stored \\o/"





if __name__ == '__main__':
    main()
