import sys

class ErrorHandler:

    def __init__(self, log):
        self.log = log

    def handle(self, msg, shellOutput=True, exitProgram=False):
        self.log.error(msg)

        if shellOutput:
            print msg
        if exitProgram:
            sys.exit(1)