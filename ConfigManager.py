import ConfigParser
import sys

class ConfigManager:

    def __init__(self):
        self.cfgParser = ConfigParser.ConfigParser()
        self.cfgParser.read('./config.ini')

    def get(self, section, keys):
        try:
            if type(keys) == str:
                return self.cfgParser.get(section, keys) 
            values = []
            for key in keys:
                values.append( self.cfgParser.get(section, key) )
        except Exception, e:
            print "ConfigManager Error: ", e
            sys.exit(1)

        return tuple(values)